# About This Profile

Use this profile to instantiate an experiment using Open Air Interface 5g nr 
(https://gitlab.eurecom.fr/oai/openairinterface5g/wikis/5g-nr-development-and-releases) 
to realize an SDR-based RAN. This profile includes the following resources:

SDR UE (d430 + USRP X300) running OAI ('rue1')
SDR eNodeB (d430 + USRP B210) running OAI ('enb1')
Powder startup scripts automatically configure OAI for the specific allocated resources. 

The following branches of OAI are used for this profile and it only support PHY-test mode.

Be sure to setup your SSH keys as outlined in the manual; it's better
to log in via a real SSH client to the nodes in your experiment.

The Open Air Interface source is located under `/opt/oai` on the enb1
and rue1 nodes.  It is mounted as a clone of a remote blockstore
(dataset) maintained by Powder.  Feel free to change anything in
here, but be aware that your changes will not persist when your
experiment terminates.


# Getting Started

After booting is complete, log onto either the `enb1` and `rue1`
nodes. From there, you will be able to start OAI enb and ue services.

Running enb service on 'enb1':

    $ sudo /local/repository/bin/enb.start.sh 

It will first load `nasmesh.ko` and set up oai0 interface, it will have IP address 10.0.1.1 netmask 255.255.255.0.

After starting the enb service you will see following logs:

![avatar](https://gitlab.flux.utah.edu/jczhu/OAI-NR/raw/master/figures/enb_log.png)

on 'rue1':

    $ sudo /local/repository/bin/ue.start.sh 

It will first load `nasmesh.ko` and set up oai0 interface, it will have IP address 10.0.1.9 netmask 255.255.255.0.

This will stop currently running OAI enb/ue service, start enb/ue service
again in a new screen session, and then show OAI processes on 'enb1'/'rue1'. 

After the UE connected to the enb you are able to see following logs:    
    
![avatar](https://gitlab.flux.utah.edu/jczhu/OAI-NR/raw/master/figures/ue_log.png)



You can detach the sesseion by ctrl-a d. 

After detach, you can pull up and monitor the OAI
processes on the `rue1` and `enb1` nodes. Execute `sudo screen -ls` to
see what sessions are available. The commands for controlling services
on these nodes are located in `/local/repository/bin`.

You can also enable DL scope ue by add '-d' at the end of line 
"screen -c $OAIETCDIR/enb.screenrc -L -S ue -h 10000 /bin/bash -c "sudo $ENBEXEPATH -U  -C 2680000000 --ue-txgain 85 --ue-rxgain 90 --ue-scan-carrier -r50 -E --phy-test -d"
in file '/local/repository/bin/ue.start.s'.

The scope will looks like this:
![avatar](https://gitlab.flux.utah.edu/jczhu/OAI-NR/raw/master/figures/UE_scope.png)

And stats:
![avatar](https://gitlab.flux.utah.edu/jczhu/OAI-NR/raw/master/figures/UE_stats.png)



OAI is a project that is in development. As such, it is not always
stable and there will be times when it gets into a failed state that
it can never recover from. Almost always, you will be able to bring
things back by either rebooting the experiment from the portal or
restarting the services by hand from the command line.

# Notes

MCS is hardcoded in 'eNB_scheduler_phytest.c'.

# Change

For more detailed information:

  * [Controlling OAI](https://gitlab.flux.utah.edu/jczhu/OAI-NR/blob/master/control.md)
  * [Inspecting OAI](https://gitlab.flux.utah.edu/jczhu/OAI-NR/blob/master/inspect.md)
  * [Modifying OAI](https://gitlab.flux.utah.edu/jczhu/OAI-NR/blob/master/modify.md)
  * [Modifying This Profile](https://gitlab.flux.utah.edu/jczhu/OAI-NR/blob/master/modify-profile.md)

# Reference
https://gitlab.eurecom.fr/oai/openairinterface5g/wikis/5g-nr-development-and-releases
https://gitlab.eurecom.fr/oai/openairinterface5g/wikis/HowToConnectOAIENBWithOAIUEWithoutS1Interface
